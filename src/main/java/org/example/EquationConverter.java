package org.example;

import org.example.domain.Equation;

public class EquationConverter {

    public static String convertEquationToString(final Equation equation, final String type) {
        String equationRepresentation;
        if ("double".equals(type) || "float".equals(type)) {
            equationRepresentation = String.format("%.2f * %.2f = %.2f", equation.getFirstMultiplier(),
                    equation.getSecondMultiplier(), equation.getProduct());
        } else {
            equationRepresentation = String.format("%.0f * %.0f = %.0f", equation.getFirstMultiplier(),
                    equation.getSecondMultiplier(), equation.getProduct());
        }
        return equationRepresentation;
    }

    private EquationConverter() {
    }
}
