package org.example;

import lombok.AllArgsConstructor;
import org.example.domain.Equation;
import org.example.domain.MultiplicationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor
public class Multiplier {

    private static final Logger LOGGER = LoggerFactory.getLogger(Multiplier.class);

    private static final Map<String, BigDecimal> MAXIMUM_VALUE = Map.of(
            "byte", BigDecimal.valueOf(Byte.MAX_VALUE),
            "short", BigDecimal.valueOf(Short.MAX_VALUE),
            "int", BigDecimal.valueOf(Integer.MAX_VALUE),
            "long", BigDecimal.valueOf(Long.MAX_VALUE),
            "double", BigDecimal.valueOf(Double.MAX_VALUE),
            "float", BigDecimal.valueOf(Float.MAX_VALUE)
    );

    public List<Equation> generateMultiplicationTable(final MultiplicationRequest multiplicationRequest) {
        if(multiplicationRequest == null) {
            throw new NullPointerException("Multiplication request can't be null");
        }
        LOGGER.info("Start generating table with next request: {}", multiplicationRequest);
        return Stream.iterate(multiplicationRequest.getMin(), firstMultiplier -> isFirstMultiplierValid(
                        firstMultiplier, multiplicationRequest), n -> n + multiplicationRequest.getIncrement())
                .flatMap(firstMultiplier -> Stream.iterate(multiplicationRequest.getMin(), secondMultiplier -> isSecondMultiplierValid(
                                firstMultiplier, secondMultiplier, multiplicationRequest), step -> step + multiplicationRequest.getIncrement())
                        .map(secondMultiplier -> new Equation(firstMultiplier, secondMultiplier, firstMultiplier * secondMultiplier)))
                .collect(Collectors.toList());
    }

    protected boolean isFirstMultiplierValid(final double firstMultiplier, final MultiplicationRequest multiplicationRequest) {
        return BigDecimal.valueOf(Math.abs(firstMultiplier)).compareTo(getTypeMaxValue(multiplicationRequest.getType())) <= 0
                && firstMultiplier <= multiplicationRequest.getMax() && firstMultiplier >= multiplicationRequest.getMin();
    }

    protected boolean isSecondMultiplierValid(final double firstMultiplier, final double secondMultiplier,
                                              final MultiplicationRequest multiplicationRequest) {
        return BigDecimal.valueOf(Math.abs(firstMultiplier * secondMultiplier)).compareTo(getTypeMaxValue(multiplicationRequest.getType())) <= 0 &&
                BigDecimal.valueOf(secondMultiplier).compareTo(getTypeMaxValue(multiplicationRequest.getType())) <= 0
                && secondMultiplier <= multiplicationRequest.getMax() && secondMultiplier >= multiplicationRequest.getMin();
    }

    protected BigDecimal getTypeMaxValue(final String type) {
        return type == null || !MAXIMUM_VALUE.containsKey(type) ?
                BigDecimal.valueOf(Integer.MAX_VALUE)
                : MAXIMUM_VALUE.get(type);
    }

}
