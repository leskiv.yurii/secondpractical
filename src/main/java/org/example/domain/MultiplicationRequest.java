package org.example.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MultiplicationRequest {

    private String type;
    private double increment;
    private double min;
    private double max;

}
