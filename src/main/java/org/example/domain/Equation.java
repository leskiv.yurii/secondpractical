package org.example.domain;

import lombok.Data;

@Data
public class Equation {

    private final double firstMultiplier;
    private final double secondMultiplier;
    private final double product;


}
