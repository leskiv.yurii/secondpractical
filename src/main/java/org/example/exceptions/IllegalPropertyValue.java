package org.example.exceptions;

public class IllegalPropertyValue  extends RuntimeException{

    public IllegalPropertyValue(String message) {
        super(message);
    }
}
