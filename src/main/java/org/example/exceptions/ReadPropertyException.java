package org.example.exceptions;

public class ReadPropertyException extends RuntimeException{

    public ReadPropertyException(Throwable cause) {
        super(cause);
    }
}
