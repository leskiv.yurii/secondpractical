package org.example;

import org.example.domain.MultiplicationRequest;
import org.example.exceptions.IllegalPropertyValue;
import org.example.exceptions.ReadPropertyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Properties;

public class PropertyReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyReader.class);
    private static final String WHOLE_TYPES = "int, long, byte, short";

    public MultiplicationRequest readProperty(final String propertyName) {
        try {
            Properties properties = new Properties();
            properties.load(Main.class.getClassLoader().getResourceAsStream(propertyName));

            LOGGER.info("Start reading properties from next property: {}", propertyName);
            String type = System.getProperty("type") == null ? "int" : System.getProperty("type");
            MultiplicationRequest multiplicationRequest = MultiplicationRequest.builder()
                    .type(type)
                    .min(Double.parseDouble(properties.getProperty("min")))
                    .max(Double.parseDouble(properties.getProperty("max")))
                    .increment(Double.parseDouble(properties.getProperty("increment")))
                    .build();
            if (isPropertiesValid(multiplicationRequest)) {
                return multiplicationRequest;
            }
            throw new IllegalPropertyValue("Values and type inside of property aren't valid: " + multiplicationRequest);
        } catch (Exception e) {
            throw new ReadPropertyException(e);
        }
    }

    private boolean isPropertiesValid(MultiplicationRequest multiplicationRequest) {
        if (WHOLE_TYPES.contains(multiplicationRequest.getType())) {
            return isNumberWhole(multiplicationRequest.getMin()) && isNumberWhole(multiplicationRequest.getMax())
                    && isNumberWhole(multiplicationRequest.getIncrement());
        }
        return true;
    }

    private boolean isNumberWhole(final double number) {
        BigDecimal currentValue = BigDecimal.valueOf(Math.abs(number));
        return currentValue.subtract(BigDecimal.valueOf(currentValue.intValue())).equals(BigDecimal.valueOf(0.0));
    }
}
