package org.example;

import org.example.domain.MultiplicationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Program was launched!");
        PropertyReader propertyReader = new PropertyReader();
        Multiplier multiplier = new Multiplier();
        MultiplicationRequest multiplicationRequest = propertyReader.readProperty("multiplication.properties");
        multiplier.generateMultiplicationTable(multiplicationRequest)
                .forEach(equation -> LOGGER.info(EquationConverter.convertEquationToString(equation, multiplicationRequest.getType())));
        LOGGER.info("Program finished execution!");
    }

}
