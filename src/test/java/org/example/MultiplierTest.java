package org.example;

import org.example.domain.Equation;
import org.example.domain.MultiplicationRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MultiplierTest {

    Multiplier multiplier = new Multiplier();

    @Test
    void testMultiplyInvalid() {
        assertThatThrownBy(()-> multiplier.generateMultiplicationTable(null))
                .hasMessage("Multiplication request can't be null")
                .isInstanceOf(NullPointerException.class);

    }

    @ParameterizedTest
    @MethodSource("generateDataIsFirstMultiplierValid")
    void testIsFirstMultiplierValid(double firstMultiplier, boolean expectedResult) {
        MultiplicationRequest multiplicationRequest = MultiplicationRequest.builder()
                .increment(1)
                .type("byte")
                .min(1)
                .max(2)
                .build();

        assertThat(multiplier.isFirstMultiplierValid(firstMultiplier, multiplicationRequest))
                .isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("generateDataIsSecondMultiplierValid")
    void testIsSecondMultiplierValid(double firstMultiplier, double secondMultiplier,  boolean expectedResult, MultiplicationRequest request) {
        assertThat(multiplier.isSecondMultiplierValid(firstMultiplier, secondMultiplier, request))
                .isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("generateDataGetMaxType")
    void getTypeMaxValue(String type, BigDecimal expectedResult) {
        assertThat(multiplier.getTypeMaxValue(type))
                .isEqualTo(expectedResult);
    }

    @Test
    void testGenerateMultiplicationTableValid() {
        MultiplicationRequest multiplicationRequest = MultiplicationRequest.builder()
                .increment(2)
                .type("byte")
                .min(2)
                .max(5)
                .build();

        assertThat(multiplier.generateMultiplicationTable(multiplicationRequest))
                .containsAll(List.of(new Equation(2, 2, 4),
                        new Equation(2, 4, 8),
                        new Equation(4, 2, 8),
                        new Equation(4, 4, 16)));
    }

    @Test
    void testGenerateMultiplicationTableValidFractions() {
        ByteArrayOutputStream actualContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(actualContent));
        MultiplicationRequest multiplicationRequest = MultiplicationRequest.builder()
                .increment(0.75)
                .type("double")
                .min(1.32)
                .max(3.12)
                .build();

        assertThat(multiplier.generateMultiplicationTable(multiplicationRequest))
                .containsAll(List.of(new Equation(1.32, 1.32, 1.7424000000000002),
                        new Equation(1.32, 2.0700000000000003, 2.7324000000000006),
                        new Equation(1.32, 2.8200000000000003, 3.7224000000000004),
                        new Equation(2.0700000000000003, 1.32, 2.7324000000000006),
                        new Equation(2.0700000000000003, 2.0700000000000003, 4.284900000000001),
                        new Equation(2.0700000000000003, 2.8200000000000003, 5.8374000000000015)));

    }

    static Stream<Arguments> generateDataIsFirstMultiplierValid() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(45, false)
        );
    }

    static Stream<Arguments> generateDataGetMaxType() {
        return Stream.of(
                Arguments.of("byte", BigDecimal.valueOf(127)),
                Arguments.of("type doesn't exist", BigDecimal.valueOf(Integer.MAX_VALUE)),
                Arguments.of(null, BigDecimal.valueOf(Integer.MAX_VALUE))
        );
    }

    static Stream<Arguments> generateDataIsSecondMultiplierValid() {
        return Stream.of(
                Arguments.of(1, 5, true,
                        MultiplicationRequest.builder()
                                .increment(1)
                                .type("byte")
                                .min(1)
                                .max(6)
                                .build()),
                Arguments.of(45, 32, false,
                        MultiplicationRequest.builder()
                                .increment(5)
                                .type("byte")
                                .min(15)
                                .max(16)
                                .build())
        );
    }
}
