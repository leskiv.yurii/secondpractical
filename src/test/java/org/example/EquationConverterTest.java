package org.example;

import org.example.domain.Equation;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class EquationConverterTest {
    @ParameterizedTest
    @MethodSource("generateDataForPrinting")
    void testPrintEquation(Equation equation, String type, String expectedResult) {
        ByteArrayOutputStream actualContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(actualContent));


        String actualResult = EquationConverter.convertEquationToString(equation, type).replaceAll("\r", "");

        assertThat(actualResult).hasToString(expectedResult);
    }

    static Stream<Arguments> generateDataForPrinting() {
        return Stream.of(
                Arguments.of(new Equation(2, 2, 4), "byte", "2 * 2 = 4"),
                Arguments.of(new Equation(3, 3, 9), "short", "3 * 3 = 9"),
                Arguments.of(new Equation(5, 5, 25), "int", "5 * 5 = 25"),
                Arguments.of(new Equation(1.6, 2.0, 3.2), "double", "1.60 * 2.00 = 3.20"),
                Arguments.of(new Equation(3.7, 2.4, 8.88),"float", "3.70 * 2.40 = 8.88")
        );
    }
}
