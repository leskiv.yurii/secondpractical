package org.example;

import org.assertj.core.api.Assertions;
import org.example.domain.MultiplicationRequest;
import org.example.exceptions.ReadPropertyException;
import org.junit.jupiter.api.Test;

class PropertyReaderTest {

    private final PropertyReader propertyReader = new PropertyReader();

    @Test
    void testReadPropertyValid() {
     System.setProperty("type", "byte");
     MultiplicationRequest multiplicationRequest = propertyReader.readProperty("testProperty.properties");

        Assertions.assertThat(multiplicationRequest)
                .returns("byte", MultiplicationRequest::getType)
                .returns(1.0, MultiplicationRequest::getMin)
                .returns(2.0, MultiplicationRequest::getMax)
                .returns(1.0, MultiplicationRequest::getIncrement);
    }

    @Test
    void testReadPropertyInvalid() {
        Assertions.assertThatThrownBy(() -> propertyReader.readProperty("notExist.properties"))
                .withFailMessage("inStream parameter is null")
                .isInstanceOf(ReadPropertyException.class);
    }

    @Test
    void testReadPropertyInvalidProperties() {
        System.setProperty("type", "byte");
        Assertions.assertThatThrownBy(() -> propertyReader.readProperty("testPropertyInvalid.properties"))
                .withFailMessage("Values and type inside of property aren't valid: MultiplicationRequest(type=byte, increment=1.0, min=1.0, max=2.1)")
                .isInstanceOf(ReadPropertyException.class);
    }
}
